# Rappel commandes

## Créer le projet
- ng new nomDuProjet

## Lancer le serveur
- ng serve → Lance le serveur mais vous devez ouvrir vous même le navigateur
- ng serve --open → Lance le serveur directement sur votre navigateur pref
- ng s --o → Pareil qu'au dessus mais version flemme

## Créer un composant
- ng generate component cheminEventuel/nomDuComponent
- ng g c cheminEventuel/nomDuComponent


Toutes les commandes possibles ici : https://angular.io/cli/generate