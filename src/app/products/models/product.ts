export interface Product {
    name : string;
    price : number;
    quantity : number;
    lastPrice : number;
    promo : boolean;
    picture : string;
}