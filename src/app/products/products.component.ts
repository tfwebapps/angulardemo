import { Component } from '@angular/core';
import { Product } from './models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {
  pizzas : Product[] = [
    { name : 'Calzone', picture : './assets/img/calzone.webp', price : 10, promo : true, lastPrice : 5.50, quantity : 2 },
    { name : 'Margherita', picture : './assets/img/margherita.webp', price : 7.50, promo : false, lastPrice : 7, quantity : 5 },
    { name : 'Quatre Fromages', picture : './assets/img/quatrefromage.jpg', price : 9.80, promo : false, lastPrice : 8, quantity : 6},
    { name : 'Regina', picture : './assets/img/regina.jpeg', price : 10, promo : true, lastPrice : 7.50, quantity : 3}
  ]

}
