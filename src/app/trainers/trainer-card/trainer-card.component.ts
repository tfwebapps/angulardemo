import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Trainer } from '../models/trainer';

@Component({
  selector: 'app-trainer-card',
  templateUrl: './trainer-card.component.html',
  styleUrls: ['./trainer-card.component.scss']
})
export class TrainerCardComponent {
  @Input() trainer! : Trainer
  @Output() onSelect : EventEmitter<string> /*= new EventEmitter<string>()*/

  constructor() {
    this.onSelect = new EventEmitter<string>()
  }

  selectTrainer() : void {
    this.onSelect.next( this.trainer.firstname )
    //this.onSelect.emit( this.trainer.firstname )
  }

}
