import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TrainersComponent } from './trainers/trainers.component';
import { ProductsComponent } from './products/products.component';
import { TrainerCardComponent } from './trainers/trainer-card/trainer-card.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TrainersComponent,
    ProductsComponent,
    TrainerCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
